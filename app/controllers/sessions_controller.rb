class SessionsController < ApplicationController
  def new
    @title = "Sign in"
  end

  def create
    user = User.authenticate(params[:session][:email], 
                             params[:session][:password])

    if user.nil?
      flash.now[:error] = "Invalid email/password combination."
      @title = "Sign in"
      render :new
    else
      sign_in user
      # This was redirecting always to the user's profile page
      #   redirect_to user
      # Now, we redirect to whatever URI was requested before the sign-in, or the default user profile
      redirect_back_or user
    end
  end

  def destroy
    sign_out
    redirect_to root_path
  end

end
